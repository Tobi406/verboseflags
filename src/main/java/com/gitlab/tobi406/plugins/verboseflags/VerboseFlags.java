package com.gitlab.tobi406.plugins.verboseflags;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Map;

public final class VerboseFlags extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);

        saveDefaultConfig();
    }
    @Override
    public void onDisable() {

    }

    @EventHandler
    public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
        onCommand(event);
    }
    @EventHandler
    public void onServerCommand(ServerCommandEvent event) {
        onCommand(event);
    }

    private void onCommand(Event event) {
        String msg = null;
        String prefix = "&7&l[&b&lV&3&lF&7&l] ";

        if (event instanceof PlayerCommandPreprocessEvent) msg = ((PlayerCommandPreprocessEvent) event).getMessage();
        if (event instanceof ServerCommandEvent) msg = ((ServerCommandEvent) event).getCommand();

        if (msg == null) return;
        try {
            if (!getServer().getPluginCommand(msg.split(" ")[0].replace("/", "")).getPlugin().getName().equals("LuckPerms")) return;
        } catch (NullPointerException e) {
            return;
        }
        String[] msgs = msg.split(" ");
        if (msgs.length < 3) return;
        if (!(msgs[1].equalsIgnoreCase("verbose") && (msgs[2].equalsIgnoreCase("on") || msgs[2].equalsIgnoreCase("record")))) return;
        if (!(getConfig().getMapList("flags").size() > 0)) return;


        String query = msg.substring(msgs[0].length() + msgs[1].length() + msgs[2].length() + 2 + (msgs.length >= 4 ? 1 : 0));

        String symbol = "#";
        query = (symbol + "default") + (query.equals("") ? "" : " & ") + query;

        List<Map<?, ?>> map = getConfig().getMapList("flags");
        for (String s : query.split(" ")) {
            if (s.startsWith(symbol)) {
                for (Map<?, ?> parsedMap : map) {
                    if (parsedMap.containsKey(s.replace(symbol, ""))) {
                        query = query.replace(s, (String) parsedMap.get(s.replace(symbol, "")));
                    }
                }
            }
        }


        String command = msgs[0] + " " + msgs[1] + " " + msgs[2] + " " + query;

        if (event instanceof PlayerCommandPreprocessEvent) ((PlayerCommandPreprocessEvent) event).setMessage(command);
        if (event instanceof ServerCommandEvent) ((ServerCommandEvent) event).setCommand(command);
    }
}
